#version 330 core

in vec2 chTex;
in vec2 chPos;
out vec4 outCol;

uniform sampler2D uTex;   //teksturna jedinica
uniform vec2 uFirePos;
uniform vec2 uFireSprite;

void main()
{
	vec4 tex = texture(uTex, (chTex + uFireSprite));
	if (tex.a < 0.05) discard;

	outCol = tex;
}
