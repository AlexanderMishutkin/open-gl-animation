#version 330 core

in vec2 chTex; //koordinate teksture
in vec4 chCol; //koordinate color
in vec2 chPos;
out vec4 outCol;

uniform sampler2D uTex; //teksturna jedinica
uniform vec3 uLightCol;
uniform vec2 uFirePos;
uniform vec3 uColMove;
uniform float uAlphaMove;

void main()
{
	vec4 tex = texture(uTex, chTex);
	if (tex == vec4(0, 0, 0, 1)) tex = vec4(chCol.rgb + uColMove, chCol.a + uAlphaMove);
	if (tex.a < 0.1) discard;

	vec3 lightCol = uLightCol;

	float fireDistance = 0.1 + length(uFirePos - chPos);
	if (fireDistance < 1.57 / 3) lightCol.r = lightCol.r + cos(3 * fireDistance);
	if (fireDistance < 1.57 / 3) lightCol.g = lightCol.g + cos(3 * fireDistance);

	outCol = tex * vec4(lightCol, 1.0);

}