//Autor: Nedeljko Tesanovic
//Opis: Primjer upotrebe tekstura

#define _CRT_SECURE_NO_WARNINGS

#include <iostream>
#include <fstream>
#include <sstream>

#include <GL/glew.h>
#include <GLFW/glfw3.h>

//stb_image.h je header-only biblioteka za ucitavanje tekstura.
//Potrebno je definisati STB_IMAGE_IMPLEMENTATION prije njenog ukljucivanja
#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

unsigned int compileShader(GLenum type, const char* source);
unsigned int createShader(const char* vsSource, const char* fsSource);
static unsigned loadImageToTexture(const char* filePath); //Ucitavanje teksture, izdvojeno u funkciju
double getRedDuringTheDay(double dayLength, double time);
double getGreenDuringTheDay(double dayLength, double time);
double getBlueDuringTheDay(double dayLength, double time);
double getSunX(double dayLength, double time);
double getSunY(double dayLength, double time);
double getMoonX(double dayLength, double time);
double getMoonY(double dayLength, double time);
double getSharkX(double speed, double time, double ampl, double faze);
double getWaterY(double dayLength, double time);

int main(void)
{

    if (!glfwInit())
    {
        std::cout<<"GLFW Biblioteka se nije ucitala! :(\n";
        return 1;
    }

    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    GLFWwindow* window;
    const char wTitle[] = "[Generic Title]";
    
    // ----------- My code ----------
    double DAY_LENGTH = 60.0;
    double FPS = 16.0;

    double cloudSpeed = 0.01;

    GLFWmonitor* monitor = glfwGetPrimaryMonitor();
    const GLFWvidmode* mode = glfwGetVideoMode(monitor);

    glfwWindowHint(GLFW_RED_BITS, mode->redBits);
    glfwWindowHint(GLFW_GREEN_BITS, mode->greenBits);
    glfwWindowHint(GLFW_BLUE_BITS, mode->blueBits);
    glfwWindowHint(GLFW_REFRESH_RATE, mode->refreshRate);

    float ratio = (float)mode->width / (float)mode->height;
    window = glfwCreateWindow(mode->width, mode->height, "My Title", monitor, NULL);
    // window = glfwCreateWindow(wWidth, wHeight, wTitle, NULL, NULL);
    // ------- End of my code -------
    
    
    if (window == NULL)
    {
        std::cout << "Prozor nije napravljen! :(\n";
        glfwTerminate();
        return 2;
    }

    glfwMakeContextCurrent(window);


    if (glewInit() != GLEW_OK)
    {
        std::cout << "GLEW nije mogao da se ucita! :'(\n";
        return 3;
    }

    unsigned int unifiedShader = createShader("basic.vert", "basic.frag");
    unsigned int fireLiveShader = createShader("basic.vert", "fire.frag");

    float skies[] = {
        //X    Y       R    G    B    A
        -2.0, -2.0,   0.6, 0.8, 0.95, 1.0,
        -2.0,  2.0,   0.6, 0.8, 0.95, 1.0,
        2.0,  -2.0,   0.6, 0.8, 0.95, 1.0,
        2.0,   2.0,   0.6, 0.8, 0.95, 1.0
    };

    float clouds[] = {
        //X    Y
        3.0,   0.0,     1.0,  0.0,
        3.0,   1.0,     1.0,  1.0,
        -1.0,  0.0,     0.0,  0.0,
        -1.0,  1.0,     0.0,  1.0
    };

    float stars[] = {
        //X    Y       R    G    B    A
        0.0, 0.6,     1.0, 1.0, 1.0, 1.0,
        0.2, 0.6,     1.0, 0.8, 1.0, 1.0,
        0.4, 0.7,     1.0, 1.0, 1.0, 1.0,
        0.5, 0.5,     0.8, 0.8, 1.0, 1.0,
        0.7, 0.5,     1.0, 0.8, 1.0, 1.0,
        0.8, 0.8,     1.0, 0.8, 0.8, 1.0,
        -0.6, 0.4,     1.0, 0.8, 0.1, 1.0,
        -0.3, 0.5,     1.0, 0.1, 0.8, 1.0
    };

    #define WATER_N 7200
    float water1[WATER_N];
    float waveWidth = 4.0 / (double)(WATER_N / 36);
    float d = 0.0;
    for (int i = 0; i < WATER_N; i += 36) {
        d += waveWidth;
        float oneWave[] = {
            //X                      Y       R    G    B    A
            -2.0             + d,  -2.0,   0.1, 0.5, 0.7,  1.0,
            -2.0             + d,   0.0,   0.1, 0.5, 0.7,  1.0,
            -2.0 + waveWidth + d,  -2.0,   0.1, 0.5, 0.7,  1.0,

            -2.0 + waveWidth + d,  -2.0,   0.1, 0.5, 0.7,  1.0,
            -2.0 + waveWidth + d,   0.0,   0.1, 0.5, 0.7,  1.0,
            -2.0             + d,   0.0,   0.1, 0.5, 0.7,  1.0
        };
        for (int j = 0; j < 36; j++) {
			water1[i + j] = oneWave[j];
		}
	}


    #define ISLAND_N 600
    float island1[ISLAND_N];
    float oneIsland[] = {
        //X     Y       R    G    B    A
        0.0,  -1.0,    0.9, 0.9, 0.3, 1.0
    };
    for (int i = 0; i < 6; i++) {
		island1[i] = oneIsland[i];
	}
    for (int i = 6; i < ISLAND_N; i += 6) {
        float d = (double)(i - ISLAND_N / 2) / (double)ISLAND_N;

        float oneIsland[] = {
			//X    Y        R    G    B    A
			d,  -d * d * 4.0,    0.9, 0.9, 0.3, 1.0
		};
		for (int j = 0; j < 6; j++) {
            island1[i + j] = oneIsland[j];
        }
    }

    float fireX = 0.2;
    float fireY = -0.65;
    float fire[] =
    {   //X    Y           S         T 
        fireX + 0.5,   fireY + 0.0,     1.0 / 8,  0.0,
        fireX + 0.5,   fireY + 1.0,     1.0 / 8,  1.0 / 4,
        fireX + 0.25,  fireY + 0.0,     0.0,      0.0,
        fireX + 0.25,  fireY + 1.0,     0.0,      1.0 / 4
    };

    float fontSize = 0.03;
    float letter[] =
    {   //X            Y           S         T 
        fontSize,    0.0,          1.0 / 8,   1.0 - 1.0 / 5,
        fontSize,    fontSize,     1.0 / 8,   1.0,
        0.0,         0.0,          0.0,       1.0 - 1.0 / 5,
        0.0,         fontSize,     0.0,       1.0
    };

    float palm1[] =
	{   //X    Y        S     T 
    	0.3,  0.0,     1.0,  0.0,
    	0.3,  0.8,     1.0,  1.0,
    	0.0,  0.0,     0.0,  0.0,
    	0.0,  0.8,     0.0,  1.0
     };

    float palm2[] =
    {   //X    Y       S         T 
        0.15,  0.0,     0.0,  0.0,
        0.15,  0.4,     0.0,  1.0,
        0.0,  0.0,     1.0,  0.0,
        0.0,  0.4,     1.0,  1.0
    };

    float log[] =
    {   //X    Y       S         T 
        0.4,  0.0,     1.0,  0.0,
        0.4,  0.2,     1.0,  1.0,
        0.0,  0.0,     0.0,  0.0,
        0.0,  0.2,     0.0,  1.0
    };

    float shark[] = {
        0.0, 0.0,      0.3, 0.3, 0.3, 1.0,
        0.1, 0.4,      0.3, 0.3, 0.3, 1.0,
        0.2, 0.0,      0.3, 0.3, 0.3, 1.0
    };

    float sunSize = 0.1;

    float sun[606] = {
        0.0, 0.0, 	1.0, 1.0, 1.0, 1.0,
    };

    for (int i = 6; i < 606; i += 6) {
        float angle = ((double)(i - 6) / 594.0 * 3.1415926 * 2.0);

        float oneSun[] = {
            std::cos(angle) / ratio * sunSize,
            std::sin(angle) * sunSize,
            1.0, 1.0, 1.0, 1.0,
        };
        for (int j = 0; j < 6; j++) {
			sun[i + j] = oneSun[j];
		}
    }

    float moon[606] = {
        1.0 / ratio * sunSize, 0.0, 	0.8, 0.9, 1.0, 1.0,
    };

    for (int i = 6; i < 606; i += 6) {
        float angle = ((double)(i - 6) / 600.0 * 3.1415926 * 2.0);

        if (angle < 3.1415926 / 2.0 || angle > 3.1415926 / 2.0 * 3.0) {
            float oneMoon[] = {
                std::cos(angle) / ratio * sunSize,
                std::sin(angle) * sunSize,
                0.8, 0.9, 1.0, 1.0,
            };

            for (int j = 0; j < 6; j++) {
                moon[i + j] = oneMoon[j];
            }
        }
        else {
            float oneMoon[] = {
                -std::cos(angle) / ratio * 0.2 * sunSize,
                std::sin(angle) * sunSize,
                0.8, 0.9, 1.0, 1.0,
            };

            for (int j = 0; j < 6; j++) {
                moon[i + j] = oneMoon[j];
            }
        }
    }

    float vertices[] =
    {   //X    Y      S    T 
        0.25, 0.0,   1.0, 0.0, //prvo tjeme
        -0.25, 0.0,  0.0, 0.0, //drugo tjeme
        0.0, 0.5,    0.5, 1.0  //trece tjeme
    };
    float vertices2[] =
    {   //X    Y     R    G    B    A
        0.25, 0.0,  1.0, 0.0, 0.0, 0.5, //prvo tjeme
        -0.25, 1.0, 1.0, 0.0, 0.0, 0.5, //drugo tjeme
        0.0, 1.5,   1.0, 0.0, 0.0, 0.5  //trece tjeme
    };
    // notacija koordinata za teksture je STPQ u GLSL-u (ali se cesto koristi UV za 2D teksture i STR za 3D)
    //ST koordinate u nizu tjemena su koordinate za teksturu i krecu se od 0 do 1, gdje je 0, 0 donji lijevi ugao teksture
    //Npr. drugi red u nizu tjemena ce da mapira boje donjeg lijevog ugla teksture na drugo tjeme

    #define N 15
    unsigned int VAO[N];
    glGenVertexArrays(N, VAO);
    unsigned int VBO[N];
    glGenBuffers(N, VBO);

    glBindVertexArray(VAO[0]);
    glBindBuffer(GL_ARRAY_BUFFER, VBO[0]);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, (2 + 2) * sizeof(float), (void*)0);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, (2 + 2) * sizeof(float), (void*)(2 * sizeof(float)));
    glEnableVertexAttribArray(1);

    glBindVertexArray(VAO[1]);
    glBindBuffer(GL_ARRAY_BUFFER, VBO[1]);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices2), vertices2, GL_STATIC_DRAW);
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, (2 + 4) * sizeof(float), (void*)0);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(2, 4, GL_FLOAT, GL_FALSE, (2 + 4) * sizeof(float), (void*)(2 * sizeof(float)));
    glEnableVertexAttribArray(2);

    glBindVertexArray(VAO[2]);
    glBindBuffer(GL_ARRAY_BUFFER, VBO[2]);
    glBufferData(GL_ARRAY_BUFFER, sizeof(fire), fire, GL_STATIC_DRAW);
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, (2 + 2) * sizeof(float), (void*)0);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, (2 + 2) * sizeof(float), (void*)(2 * sizeof(float)));
    glEnableVertexAttribArray(1);

    glBindVertexArray(VAO[3]);
    glBindBuffer(GL_ARRAY_BUFFER, VBO[3]);
    glBufferData(GL_ARRAY_BUFFER, sizeof(skies), skies, GL_STATIC_DRAW);
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, (2 + 4) * sizeof(float), (void*)0);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(2, 4, GL_FLOAT, GL_FALSE, (2 + 4) * sizeof(float), (void*)(2 * sizeof(float)));
    glEnableVertexAttribArray(2);

    glBindVertexArray(VAO[4]);
    glBindBuffer(GL_ARRAY_BUFFER, VBO[4]);
    glBufferData(GL_ARRAY_BUFFER, sizeof(water1), water1, GL_STATIC_DRAW);
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, (2 + 4) * sizeof(float), (void*)0);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(2, 4, GL_FLOAT, GL_FALSE, (2 + 4) * sizeof(float), (void*)(2 * sizeof(float)));
    glEnableVertexAttribArray(2);

    glBindVertexArray(VAO[5]);
    glBindBuffer(GL_ARRAY_BUFFER, VBO[5]);
    glBufferData(GL_ARRAY_BUFFER, sizeof(island1), island1, GL_STATIC_DRAW);
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, (2 + 4) * sizeof(float), (void*)0);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(2, 4, GL_FLOAT, GL_FALSE, (2 + 4) * sizeof(float), (void*)(2 * sizeof(float)));
    glEnableVertexAttribArray(2);

    glBindVertexArray(VAO[6]);
    glBindBuffer(GL_ARRAY_BUFFER, VBO[6]);
    glBufferData(GL_ARRAY_BUFFER, sizeof(palm1), palm1, GL_STATIC_DRAW);
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, (2 + 2) * sizeof(float), (void*)0);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, (2 + 2) * sizeof(float), (void*)(2 * sizeof(float)));
    glEnableVertexAttribArray(1);

    glBindVertexArray(VAO[7]);
    glBindBuffer(GL_ARRAY_BUFFER, VBO[7]);
    glBufferData(GL_ARRAY_BUFFER, sizeof(palm2), palm2, GL_STATIC_DRAW);
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, (2 + 2) * sizeof(float), (void*)0);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, (2 + 2) * sizeof(float), (void*)(2 * sizeof(float)));
    glEnableVertexAttribArray(1);

    glBindVertexArray(VAO[8]);
    glBindBuffer(GL_ARRAY_BUFFER, VBO[8]);
    glBufferData(GL_ARRAY_BUFFER, sizeof(log), log, GL_STATIC_DRAW);
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, (2 + 2) * sizeof(float), (void*)0);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, (2 + 2) * sizeof(float), (void*)(2 * sizeof(float)));
    glEnableVertexAttribArray(1);

    glBindVertexArray(VAO[9]);
    glBindBuffer(GL_ARRAY_BUFFER, VBO[9]);
    glBufferData(GL_ARRAY_BUFFER, sizeof(shark), shark, GL_STATIC_DRAW);
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, (2 + 4) * sizeof(float), (void*)0);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(2, 4, GL_FLOAT, GL_FALSE, (2 + 4) * sizeof(float), (void*)(2 * sizeof(float)));
    glEnableVertexAttribArray(2);

    glBindVertexArray(VAO[10]);
    glBindBuffer(GL_ARRAY_BUFFER, VBO[10]);
    glBufferData(GL_ARRAY_BUFFER, sizeof(sun), sun, GL_STATIC_DRAW);
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, (2 + 4) * sizeof(float), (void*)0);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(2, 4, GL_FLOAT, GL_FALSE, (2 + 4) * sizeof(float), (void*)(2 * sizeof(float)));
    glEnableVertexAttribArray(2);

    glBindVertexArray(VAO[11]);
    glBindBuffer(GL_ARRAY_BUFFER, VBO[11]);
    glBufferData(GL_ARRAY_BUFFER, sizeof(moon), moon, GL_STATIC_DRAW);
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, (2 + 4) * sizeof(float), (void*)0);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(2, 4, GL_FLOAT, GL_FALSE, (2 + 4) * sizeof(float), (void*)(2 * sizeof(float)));
    glEnableVertexAttribArray(2);

    glBindVertexArray(VAO[12]);
    glBindBuffer(GL_ARRAY_BUFFER, VBO[12]);
    glBufferData(GL_ARRAY_BUFFER, sizeof(letter), letter, GL_STATIC_DRAW);
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, (2 + 2) * sizeof(float), (void*)0);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, (2 + 2) * sizeof(float), (void*)(2 * sizeof(float)));
    glEnableVertexAttribArray(1);

    glBindVertexArray(VAO[13]);
    glBindBuffer(GL_ARRAY_BUFFER, VBO[13]);
    glBufferData(GL_ARRAY_BUFFER, sizeof(clouds), clouds, GL_STATIC_DRAW);
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, (2 + 2) * sizeof(float), (void*)0);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, (2 + 2) * sizeof(float), (void*)(2 * sizeof(float)));
    glEnableVertexAttribArray(1);

    glBindVertexArray(VAO[14]);
    glBindBuffer(GL_ARRAY_BUFFER, VBO[14]);
    glBufferData(GL_ARRAY_BUFFER, sizeof(stars), stars, GL_STATIC_DRAW);
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, (2 + 4) * sizeof(float), (void*)0);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(2, 4, GL_FLOAT, GL_FALSE, (2 + 4) * sizeof(float), (void*)(2 * sizeof(float)));
    glEnableVertexAttribArray(2);

    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);
    
    //Tekstura
    unsigned checkerTexture = loadImageToTexture("res/texel_checker.png"); //Ucitavamo teksturu
    glBindTexture(GL_TEXTURE_2D, checkerTexture); //Podesavamo teksturu
    glGenerateMipmap(GL_TEXTURE_2D); //Generisemo mipmape 
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);//S = U = X    GL_REPEAT, GL_CLAMP_TO_EDGE, GL_CLAMP_TO_BORDER
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);// T = V = Y
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);   //GL_NEAREST, GL_LINEAR
    glBindTexture(GL_TEXTURE_2D, 0);

    unsigned cloudsTexture = loadImageToTexture("res/Clouds.png"); //Ucitavamo teksturu
    glBindTexture(GL_TEXTURE_2D, cloudsTexture); //Podesavamo teksturu
    glGenerateMipmap(GL_TEXTURE_2D); //Generisemo mipmape 
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);//S = U = X    GL_REPEAT, GL_CLAMP_TO_EDGE, GL_CLAMP_TO_BORDER
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);// T = V = Y
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);   //GL_NEAREST, GL_LINEAR
    glBindTexture(GL_TEXTURE_2D, 0);

    unsigned palmTexture = loadImageToTexture("res/palm.png"); //Ucitavamo teksturu
    glBindTexture(GL_TEXTURE_2D, palmTexture); //Podesavamo teksturu
    glGenerateMipmap(GL_TEXTURE_2D); //Generisemo mipmape 
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);//S = U = X    GL_REPEAT, GL_CLAMP_TO_EDGE, GL_CLAMP_TO_BORDER
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);// T = V = Y
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);   //GL_NEAREST, GL_LINEAR
    glBindTexture(GL_TEXTURE_2D, 0);

    unsigned logTexture = loadImageToTexture("res/log.png"); //Ucitavamo teksturu
    glBindTexture(GL_TEXTURE_2D, logTexture); //Podesavamo teksturu
    glGenerateMipmap(GL_TEXTURE_2D); //Generisemo mipmape 
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);//S = U = X    GL_REPEAT, GL_CLAMP_TO_EDGE, GL_CLAMP_TO_BORDER
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);// T = V = Y
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);   //GL_NEAREST, GL_LINEAR
    glBindTexture(GL_TEXTURE_2D, 0);

    unsigned fireTexture = loadImageToTexture("res/fire_sprite_8x4_512x512_1.png");
    glBindTexture(GL_TEXTURE_2D, fireTexture);
    glGenerateMipmap(GL_TEXTURE_2D);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);//S = U = X    GL_REPEAT, GL_CLAMP_TO_EDGE, GL_CLAMP_TO_BORDER
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);// T = V = Y
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);   //GL_NEAREST, GL_LINEAR
    glBindTexture(GL_TEXTURE_2D, 0);

    unsigned letterTexture = loadImageToTexture("res/alphabet_8x5.png");
    glBindTexture(GL_TEXTURE_2D, letterTexture);
    glGenerateMipmap(GL_TEXTURE_2D);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);//S = U = X    GL_REPEAT, GL_CLAMP_TO_EDGE, GL_CLAMP_TO_BORDER
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);// T = V = Y
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);   //GL_NEAREST, GL_LINEAR
    glBindTexture(GL_TEXTURE_2D, 0);

    unsigned uTexLoc = glGetUniformLocation(unifiedShader, "uTex");
    glUniform1i(uTexLoc, 0); // Indeks teksturne jedinice (sa koje teksture ce se citati boje)
    unsigned uTexLoc2 = glGetUniformLocation(fireLiveShader, "uTex");
    glUniform1i(uTexLoc2, 0); // Indeks teksturne jedinice (sa koje teksture ce se citati boje)
    //Odnosi se na glActiveTexture(GL_TEXTURE0) u render petlji
    //Moguce je sabirati indekse, tj GL_TEXTURE5 se moze dobiti sa GL_TEXTURE0 + 5 , sto je korisno za iteriranje kroz petlje

    unsigned int uSunCol = glGetUniformLocation(unifiedShader, "uLightCol");
    unsigned int uFirePos = glGetUniformLocation(unifiedShader, "uFirePos");
    unsigned int uFireSprite = glGetUniformLocation(fireLiveShader, "uFireSprite");
    unsigned int uMoveText = glGetUniformLocation(fireLiveShader, "uMove");
    unsigned int uMove = glGetUniformLocation(unifiedShader, "uMove");
    unsigned int uColMove = glGetUniformLocation(unifiedShader, "uColMove");
    unsigned int uAlphaMove = glGetUniformLocation(unifiedShader, "uAlphaMove");
    unsigned int uWavingDensity = glGetUniformLocation(unifiedShader, "uWavingDensity");
    unsigned int uWaveFaze = glGetUniformLocation(unifiedShader, "uWaveFaze");
    unsigned int uWaveH = glGetUniformLocation(unifiedShader, "uWaveH");

    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glfwSetTime(0);

    int ispriteI = 0;
    int old_t = 0;

    while (!glfwWindowShouldClose(window))
    {
        double t = glfwGetTime();
        
        if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
        {
            glfwSetWindowShouldClose(window, GL_TRUE);
        }

        // ----------- My code ----------
        if (glfwGetKey(window, GLFW_KEY_1) == GLFW_PRESS)
        {
            glPolygonMode(GL_FRONT_AND_BACK, GL_POINT);
            glPointSize(8.0);
        }
        else {
            glPointSize(4.0);
        }

        if (glfwGetKey(window, GLFW_KEY_2) == GLFW_PRESS)
        {
            glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
            glLineWidth(4.0);
        }

        if (glfwGetKey(window, GLFW_KEY_3) == GLFW_PRESS)
        {
            glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

        }

        if (glfwGetKey(window, GLFW_KEY_R) == GLFW_PRESS)
        {
            glfwSetTime(0);
        }
        
        glClearColor(0.5, 0.5, 0.5, 1.0);
        glClear(GL_COLOR_BUFFER_BIT);

        glUseProgram(unifiedShader);
        glUniform1f(uWaveH, 0);
        glUniform3f(uSunCol, getRedDuringTheDay(DAY_LENGTH, t), getGreenDuringTheDay(DAY_LENGTH, t), getBlueDuringTheDay(DAY_LENGTH, t));
        glUniform2f(uFirePos, fireX + 0.375, fireY + 0.2);

        // Skies
        glBindTexture(GL_TEXTURE_2D, 0);
        glBindVertexArray(VAO[3]);
        glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

        // Sun
        glUniform3f(uSunCol, getRedDuringTheDay(DAY_LENGTH, t) + 0.3, getGreenDuringTheDay(DAY_LENGTH, t) + 0.2, getBlueDuringTheDay(DAY_LENGTH, t));
        glBindTexture(GL_TEXTURE_2D, 0);
        glUniform2f(uMove, getSunX(DAY_LENGTH, t), getSunY(DAY_LENGTH, t));
        glBindVertexArray(VAO[10]);
        glDrawArrays(GL_TRIANGLE_FAN, 0, 101);

        // Moon
        glUniform3f(uSunCol, 1.0, 1.0, 1.0);
        glBindTexture(GL_TEXTURE_2D, 0);
        glUniform2f(uMove, getMoonX(DAY_LENGTH, t), getMoonY(DAY_LENGTH, t));
        glBindVertexArray(VAO[11]);
        glDrawArrays(GL_TRIANGLE_FAN, 0, 101);
        glUniform2f(uMove, 0.0, 0.0);

        // Stars
        glBindVertexArray(VAO[14]);

        glUniform1f(uAlphaMove, -std::cos(t / DAY_LENGTH * 3.1415 * 2.0));
        glDrawArrays(GL_POINTS, 0, 8);
        glUniform1f(uAlphaMove, 0.0);

        glUniform3f(uSunCol, getRedDuringTheDay(DAY_LENGTH, t), getGreenDuringTheDay(DAY_LENGTH, t), getBlueDuringTheDay(DAY_LENGTH, t));

        // Couds
        glBindTexture(GL_TEXTURE_2D, cloudsTexture);
        glBindVertexArray(VAO[13]);
        glUniform2f(uMove, std::fmod(t * cloudSpeed, 8.0) - 4.0, 0.0);
        glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
        glUniform2f(uMove, std::fmod(t * cloudSpeed + 4.0, 8.0) - 4.0, 0.0);
        glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
        glUniform2f(uMove, 0.0, 0.0);

        // Water Far
        glUniform1f(uWavingDensity, 40);
        glUniform1f(uWaveFaze, t * 0.5);
        glUniform1f(uWaveH, 0.01);
        glBindTexture(GL_TEXTURE_2D, 0);
        glBindVertexArray(VAO[4]);
        glDrawArrays(GL_TRIANGLES, 0, WATER_N / 6);
        glUniform1f(uWaveH, 0);

        // Island
        glBindTexture(GL_TEXTURE_2D, 0);
        glBindVertexArray(VAO[5]);
        glUniform2f(uMove, -0.5, -0.1);
        glDrawArrays(GL_TRIANGLE_FAN, 0, ISLAND_N / 6);

        // Palm
        glUniform2f(uMove, -0.55, -0.15);
        glBindTexture(GL_TEXTURE_2D, palmTexture);
        glBindVertexArray(VAO[7]);
        glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
        glUniform2f(uMove, 0.0, 0.0);

        // Shark
        glUniform2f(uMove, getSharkX(1.0, t, 0.4, 0.0) + 0.2, -0.6 + getWaterY(DAY_LENGTH, t) * 0.5);
        glBindTexture(GL_TEXTURE_2D, 0);
        glBindVertexArray(VAO[9]);
        glDrawArrays(GL_TRIANGLES, 0, 3);
        glUniform2f(uMove, 0.0, 0.0);

        // Shark
        glUniform2f(uMove, getSharkX(1.0, t, 0.3, 1.56) + 0.4, -0.6 + getWaterY(DAY_LENGTH, t) * 0.5);
        glBindTexture(GL_TEXTURE_2D, 0);
        glBindVertexArray(VAO[9]);
        glDrawArrays(GL_TRIANGLES, 0, 3);
        glUniform2f(uMove, 0.0, 0.0);

        // Water Mid
        glUniform1f(uWavingDensity, 20);
        glUniform1f(uWaveFaze, 0.1 + t * 0.5);
        glUniform1f(uWaveH, 0.015);
        glUniform3f(uColMove, -0.1, -0.1, -0.1);
        glUniform2f(uMove, 0.0, -0.3 + getWaterY(DAY_LENGTH, t) * 0.5);
        glBindTexture(GL_TEXTURE_2D, 0);
        glBindVertexArray(VAO[4]);
        glDrawArrays(GL_TRIANGLES, 0, WATER_N / 6);
        glUniform1f(uWaveH, 0);
        glUniform3f(uColMove, 0, 0, 0);

        // Island Near
        glBindTexture(GL_TEXTURE_2D, 0);
        glUniform2f(uMove, 0.5, -0.3);
        glBindVertexArray(VAO[5]);
        glDrawArrays(GL_TRIANGLE_FAN, 0, ISLAND_N / 6);

        // Palm
        glUniform2f(uMove, 0.22, -0.42);
        glBindTexture(GL_TEXTURE_2D, palmTexture);
        glBindVertexArray(VAO[6]);
        glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
        glUniform2f(uMove, 0.0, 0.0);

        // Shark
        glUniform2f(uMove, getSharkX(1.0, t, 0.3, 3.14) - 0.4, -0.9 + getWaterY(DAY_LENGTH, t));
        glBindTexture(GL_TEXTURE_2D, 0);
        glBindVertexArray(VAO[9]);
        glDrawArrays(GL_TRIANGLES, 0, 3);
        glUniform2f(uMove, 0.0, 0.0);

        // Logs
        glUniform2f(uMove, 0.38, -0.62);
        glBindTexture(GL_TEXTURE_2D, logTexture);
        glBindVertexArray(VAO[8]);
        glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
        glUniform2f(uMove, 0.0, 0.0);

        // Water Near
        glUniform1f(uWavingDensity, 10);
        glUniform1f(uWaveFaze, 0.2 + t * 0.5);
        glUniform1f(uWaveH, 0.03);
        glUniform3f(uColMove, -0.3, -0.3, -0.3);
        glUniform2f(uMove, 0.0, -0.7 + getWaterY(DAY_LENGTH, t));
        glBindTexture(GL_TEXTURE_2D, 0);
        glBindVertexArray(VAO[4]);
        glDrawArrays(GL_TRIANGLES, 0, WATER_N / 6);
        glUniform1f(uWaveH, 0);
        glUniform3f(uColMove, 0, 0, 0);
        glUniform2f(uMove, 0.0, 0.0);

        // Fire
        glUseProgram(fireLiveShader);
        glBindTexture(GL_TEXTURE_2D, fireTexture);
        glBindVertexArray(VAO[2]);
        glUniform2f(uFireSprite, (1.0 / 8.0) * (double) (ispriteI % 8), 1.0 - (1.0 / 4.0) * (double)(ispriteI / 8 % 4));
        glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
        if (((int)(t * FPS)) > old_t) {
			old_t = (int) (t * FPS);
			ispriteI++;
		}

        // Text
        char name[] = "ALEKSANDR MISHUTKIN SV82";
        float shiftX = 0.0;
        for (char c : name) {
            if (c != ' ' && c != '\0') {
                int i = 38;
                if (c >= 'A' && c <= 'Z') i = c - 'A';
                if (c >= '0' && c <= '9') i = 26 + c - '0';
                glBindTexture(GL_TEXTURE_2D, letterTexture);
                glBindVertexArray(VAO[12]);

                glUniform2f(uFireSprite, (1.0 / 8.0) * (double)(i % 8), 1.0 - (1.0 / 5.0) * (double)(i / 8 % 5));
                glUniform2f(uMoveText, -0.95 + shiftX, 0.9);
                glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
            }
            shiftX += fontSize * 0.7;
        }
        glUniform2f(uMoveText, 0.0, 0.0);

        glBindVertexArray(0);
        glUseProgram(0);
        glfwSwapBuffers(window);
        glfwPollEvents();
    }


    glDeleteTextures(1, &checkerTexture);
    glDeleteTextures(1, &fireTexture);
    glDeleteTextures(1, &palmTexture);
    glDeleteTextures(1, &logTexture);
    glDeleteTextures(1, &letterTexture);
    glDeleteTextures(1, &cloudsTexture);
    glDeleteBuffers(N, VBO);
    glDeleteVertexArrays(N, VAO);
    glDeleteProgram(unifiedShader);
    glDeleteProgram(fireLiveShader);

    glfwTerminate();
    return 0;
}

unsigned int compileShader(GLenum type, const char* source)
{
    std::string content = "";
    std::ifstream file(source);
    std::stringstream ss;
    if (file.is_open())
    {
        ss << file.rdbuf();
        file.close();
        std::cout << "Uspjesno procitao fajl sa putanje \"" << source << "\"!" << std::endl;
    }
    else {
        ss << "";
        std::cout << "Greska pri citanju fajla sa putanje \"" << source << "\"!" << std::endl;
    }
     std::string temp = ss.str();
     const char* sourceCode = temp.c_str();

    int shader = glCreateShader(type);
    
    int success;
    char infoLog[512];
    glShaderSource(shader, 1, &sourceCode, NULL);
    glCompileShader(shader);

    glGetShaderiv(shader, GL_COMPILE_STATUS, &success);
    if (success == GL_FALSE)
    {
        glGetShaderInfoLog(shader, 512, NULL, infoLog);
        if (type == GL_VERTEX_SHADER)
            printf("VERTEX");
        else if (type == GL_FRAGMENT_SHADER)
            printf("FRAGMENT");
        printf(" sejder ima gresku! Greska: \n");
        printf(infoLog);
    }
    return shader;
}
unsigned int createShader(const char* vsSource, const char* fsSource)
{
    
    unsigned int program;
    unsigned int vertexShader;
    unsigned int fragmentShader;

    program = glCreateProgram();

    vertexShader = compileShader(GL_VERTEX_SHADER, vsSource);
    fragmentShader = compileShader(GL_FRAGMENT_SHADER, fsSource);

    
    glAttachShader(program, vertexShader);
    glAttachShader(program, fragmentShader);

    glLinkProgram(program);
    glValidateProgram(program);

    int success;
    char infoLog[512];
    glGetProgramiv(program, GL_VALIDATE_STATUS, &success);
    if (success == GL_FALSE)
    {
        glGetShaderInfoLog(program, 512, NULL, infoLog);
        std::cout << "Objedinjeni sejder ima gresku! Greska: \n";
        std::cout << infoLog << std::endl;
    }

    glDetachShader(program, vertexShader);
    glDeleteShader(vertexShader);
    glDetachShader(program, fragmentShader);
    glDeleteShader(fragmentShader);

    return program;
}
static unsigned loadImageToTexture(const char* filePath) {
    int TextureWidth;
    int TextureHeight;
    int TextureChannels;
    unsigned char* ImageData = stbi_load(filePath, &TextureWidth, &TextureHeight, &TextureChannels, 0);
    if (ImageData != NULL)
    {
        //Slike se osnovno ucitavaju naopako pa se moraju ispraviti da budu uspravne
        stbi__vertical_flip(ImageData, TextureWidth, TextureHeight, TextureChannels);

        // Provjerava koji je format boja ucitane slike
        GLint InternalFormat = -1;
        switch (TextureChannels) {
        case 1: InternalFormat = GL_RED; break;
        case 3: InternalFormat = GL_RGB; break;
        case 4: InternalFormat = GL_RGBA; break;
        default: InternalFormat = GL_RGB; break;
        }

        unsigned int Texture;
        glGenTextures(1, &Texture);
        glBindTexture(GL_TEXTURE_2D, Texture);
        glTexImage2D(GL_TEXTURE_2D, 0, InternalFormat, TextureWidth, TextureHeight, 0, InternalFormat, GL_UNSIGNED_BYTE, ImageData);
        glBindTexture(GL_TEXTURE_2D, 0);
        // oslobadjanje memorije zauzete sa stbi_load posto vise nije potrebna
        stbi_image_free(ImageData);
        return Texture;
    }
    else
    {
        std::cout << "Textura nije ucitana! Putanja texture: " << filePath << std::endl;
        stbi_image_free(ImageData);
        return 0;
    }
}

#define PI 3.14159265358979323846

double getRedDuringTheDay(double dayLength, double time) {
	return (std::sin((time + dayLength / 5) / dayLength * 2 * PI) + 1) / 2;
}
double getGreenDuringTheDay(double dayLength, double time) {
    return std::pow((std::sin((time + dayLength / 5) / dayLength * 2 * PI) + 1) / 2, 3);
}
double getBlueDuringTheDay(double dayLength, double time) {
    return 0.5 + (std::sin((time + dayLength / 5) / dayLength * 2 * PI) + 1) / 4;
}
double getSunX(double dayLength, double time) {
    float angle = std::fmod(time + dayLength / 4.0, dayLength) / dayLength * 2.0 * PI;
    return std::cos(angle) * 0.5;
}
double getSunY(double dayLength, double time) {
    float angle = std::fmod(time + dayLength / 4.0, dayLength) / dayLength * 2.0 * PI;
    return std::sin(angle) * 0.5;
}
double getMoonX(double dayLength, double time) {
    float angle = std::fmod(time + 3.0 * dayLength / 4.0, dayLength) / dayLength * 2.0 * PI;
    return std::cos(angle) * 0.5;
}
double getMoonY(double dayLength, double time) {
    float angle = std::fmod(time + 3.0 * dayLength / 4.0, dayLength) / dayLength * 2.0 * PI;
    return std::sin(angle) * 0.5;
}
double getSharkX(double speed, double time, double ampl, double faze) {
    return std::sin((time + faze)*speed) * ampl;
}
double getWaterY(double dayLength, double time) {
    float angle = std::fmod(time + 3.0 * dayLength / 4.0, dayLength) / dayLength * 2.0 * PI;
    return std::sin(angle) * 0.1;
}
