#version 330 core 

layout(location = 0) in vec2 inPos;
layout(location = 1) in vec2 inTex;
layout(location = 2) in vec4 inCol;
out vec2 chTex;
out vec4 chCol;
out vec2 chPos;

uniform vec2 uMove;
uniform float uWavingDensity;
uniform float uWaveFaze;
uniform float uWaveH;


void main()
{
	vec2 resPos = inPos + uMove;
	resPos.y = resPos.y + uWaveH * sin(resPos.x * uWavingDensity + uWaveFaze);

	gl_Position = vec4(resPos.x, resPos.y, 0.0, 1.0);
	chTex = inTex;
	chCol = inCol;
	chPos = resPos;
}